if (!process.env.PORT)
  process.env.PORT = 8080;

// Priprava strežnika
var express = require('express');
var expressSession = require('express-session');

var streznik = express();
streznik.use(
  expressSession({
    secret: '1234567890QWERTY', // Skrivni ključ za podpisovanje piškotkov
    saveUninitialized: true,    // Novo sejo shranimo
    resave: false,              // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 2000              // Seja poteče po 2s neaktivnosti
    }
  })
);

var stDostopov = 0;

streznik.get('/', function(zahteva, odgovor) {
  stDostopov++;
  if (zahteva.session.stDostopov) {
    zahteva.session.stDostopov++;
  } else {
    zahteva.session.stDostopov = 1;
  }
  odgovor.send('<h1>Globalne spremenljivke vs. seja</h1>' +
    '<p style="font-size:150%">' + 'To je že <strong>' +
    stDostopov + 'x</strong> dostop do strežnika, ' +
    'medtem ko ste v tej seji dostopali <strong>' +
    zahteva.session.stDostopov + 'x</strong>.' +
    '</p>');
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik pognan!");
})
